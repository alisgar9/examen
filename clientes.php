<?php
require('inc/config.php');
require('inc/functions.php');



if(!empty($_POST) && $_POST['action']=='add'){
    $Return = array('result'=>array(), 'error'=>'');

    $name = safe_input($con, $_POST['Name']);
    $email = safe_input($con, $_POST['Email']);
    $telefono = safe_input($con, $_POST['Telefono']);



    if($name===''){
        $Return['error'] = "El campo Nombre es Requerido.";
    }elseif (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
        $Return['error'] = "Correo electronico invalido.";
    }elseif($telefono===''){
        $Return['error'] = "Telefono requerido.";
    }
    if($Return['error']!=''){
        output($Return);
    }
   
    
    $result = mysqli_query($con, "SELECT * FROM tbl_clientes WHERE email='$email' LIMIT 1");
    if(mysqli_num_rows($result)==1){
        $Return['error'] = 'Cliente Duplicado.';

    }else{
        mysqli_query($con, "INSERT INTO tbl_clientes (nombre, telefono, email) values('$name','$telefono','$email')");
        mysqli_insert_id($con);
        header("Location: ./");

    }
    output($Return);
}

if(!empty($_POST) && $_POST['action']=='edit'){
    $Return = array('result'=>array(), 'error'=>'');

    $name = safe_input($con, $_POST['Name']);
    $email = safe_input($con, $_POST['Email']);
    $telefono = safe_input($con, $_POST['Telefono']);
    $id_cliente = safe_input($con, $_POST['id_cliente']);



    if($name===''){
        $Return['error'] = "El campo Nombre es Requerido.";
    }elseif (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
        $Return['error'] = "Correo electronico invalido.";
    }elseif($telefono===''){
        $Return['error'] = "Telefono requerido.";
    }elseif($id_cliente===''){
        $Return['error'] = "el cliente no existe.";
    }
    if($Return['error']!=''){
        output($Return);
    }
   
    
    $result = mysqli_query($con, "SELECT * FROM tbl_clientes WHERE id='$id_cliente' LIMIT 1");
    if(mysqli_num_rows($result)==1){

        mysqli_query($con, "UPDATE tbl_clientes SET nombre='$name', telefono='$telefono', email='$email' WHERE id = '$id_cliente'");
        header("Location: ./");
    }else{
        $Return['error'] = 'Cliente no Existe.';  

    }
    output($Return);
}

if(!empty($_POST) && $_POST['action']=='delete'){
    $Return = array('result'=>array(), 'error'=>'');

   
    $id_cliente = safe_input($con, $_POST['id_cliente']);



    if($id_cliente===''){
        $Return['error'] = "el cliente no existe.";
    }
    if($Return['error']!=''){
        output($Return);
    }
   
    
    $result = mysqli_query($con, "SELECT * FROM tbl_clientes WHERE id='$id_cliente' LIMIT 1");
    if(mysqli_num_rows($result)==1){

        mysqli_query($con, "DELETE FROM tbl_clientes  WHERE id = '$id_cliente'");
        header("Location: ./");

    }else{
        $Return['error'] = 'Cliente no Existe.';  

    }
    output($Return);
}

?>