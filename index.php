<?php
/*PHP Login and registration script Version 1.0, Created by Gautam, www.w3schools.in*/
require('inc/config.php');
require('inc/functions.php');

/*Check for authentication otherwise user will be redirects to main.php page.*/
if (!isset($_SESSION['UserData'])) {
    exit(header("location:main.php"));
}

require('include/header.php');
?>
<!-- container -->
<div class="container">


<a href="logout.php">Cerrar Sesion</a>.
<?php
$data = get_clientes($con);

?>


<div class="row">
<div class="col-md-12"><h3>Catalogo de Clientes</h3></div>
        <table class="table table-dark">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Telefono</th>
            <th scope="col">Email</th>
            <th scope="col">Acciones</th>
            </tr>
        </thead>
        <tbody>
        <?php 
          foreach($data as $value)
          {
            $liga = "edit.php?idcliente=".$value["id"];
            echo '<tr>';
            echo '<th scope="row">'.$value['id'].'</th>';
            echo '<td>'.$value['nombre'].'</td>';
            echo '<td>'.$value['telefono'].'</td>';
            echo '<td>'.$value['email'].'</td>';
            echo '<td><a href="'.$liga.'" class="btn btn-default btn-block" ><i class="fa fa-pencil" aria-hidden="true"></i></a>
            <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-idemp="'.$value["id"].'" data-target="#delete_modal'.$value["id"].'"><i class="fa fa-trash" aria-hidden="true"></i></button>
            </td>';
            echo '</tr>';



            echo '
                    <!-- Delete Model -->
                    <div id="delete_modal'.$value["id"].'" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
                      aria-hidden="true">
                      <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                  
                          <!-- header modal -->
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mySmallModalLabel">Eliminar Cliente</h4>
                          </div>
                  
                          <!-- body modal -->
                          <div class="modal-body text-center">
                            ¿Por favor, confirme que desea eliminar el cliente: <b><span id="fav-title"></span></b>?
                            <hr>
                            <form action="clientes.php" method="post" name="delete_form" id="delete_form" autocomplete="off">
                              <input name="id_cliente" value="'.$value["id"].'" type="hidden" />
                              <input name="action" value="delete" type="hidden" />
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                              <button type="submit" value="delete" class="btn btn-success">Aceptar</button>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>';
          }
        ?>
            
        </tbody>
        </table>
          
</div>
<button type="button" class="btn btn-lg btn-info btn-block" data-toggle="modal" data-target="#registration_modal">Nuevo Cliente</button>




<!-- Registration Form -->
<div class="modal fade" role="dialog" id="registration_modal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- HTML Form -->
        <form action="clientes.php" method="post" name="registration_form" id="registration_form" autocomplete="off">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Registro de Cliente Nuevo</h4>
        </div>
        <!-- Modal Body -->
        <div class="modal-body">
            <div class="form-group">
                <label for="Name">Nombre Completo</label>
                <input type="text" name="Name" id="Name" class="form-control" required pattern=".{2,100}" title="min 2 caracteres." autofocus>
            </div>
            <div class="form-group">
                <label for="email">Correo Electronico</label>
                <input type="email" name="Email" id="Email" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="Telefono">Telefono</label>
                <input type="text" name="Telefono" id="Telefono" class="form-control" required title="8 a 10 caracteres.">
            </div>
                <div id="display_error" class="alert alert-danger fade in"></div>
        </div>

        <!-- Modal Footer -->
        <div class="modal-footer">
        <input type="submit" class="btn btn-lg btn-success" value="Agregar" id="submit">
        <input type="hidden" name="action" value="add">
          <button type="button" class="btn  btn-lg  btn-default" data-dismiss="modal">Cancelar</button>
        </div>
        </form>

      </div>
    </div>
  </div>




          
  



</div>
<!-- /container -->




<?php require('include/footer.php');?>