<?php
/*PHP Login and registration script Version 1.0, Created by Gautam, www.w3schools.in*/
require('inc/config.php');
require('inc/functions.php');

/*Check for authentication otherwise user will be redirects to main.php page.*/
if (!isset($_SESSION['UserData'])) {
    exit(header("location:main.php"));
}

require('include/header.php');
?>
<!-- container -->
<div class="container">
<?php 
$data = get_cliente($con,$_GET['idcliente']);

?>



<div class="row">


<div class="col-md-12"><h3>Catalogo de Clientes</h3></div>



<form action="clientes.php" method="post" name="registration_form" id="registration_form" autocomplete="off">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Registro de Cliente Nuevo</h4>
        </div>
        <!-- Modal Body -->
        <div class="modal-body">
            <div class="form-group">
                <label for="Name">Nombre Completo</label>
                <input type="text" value="<?php echo $data['nombre']; ?>" name="Name" id="Name" class="form-control" required pattern=".{2,100}" title="min 2 caracteres." autofocus>
            </div>
            <div class="form-group">
                <label for="email">Correo Electronico</label>
                <input type="email" value="<?php echo $data['email']; ?>" name="Email" id="Email" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="Telefono">Telefono</label>
                <input type="text" value="<?php echo $data['telefono']; ?>" name="Telefono" id="Telefono" class="form-control" required title="8 a 10 caracteres.">
            </div>
                <div id="display_error" class="alert alert-danger fade in"></div>
        </div>

        <!-- Modal Footer -->
        <div class="modal-footer">
        <input type="submit" class="btn btn-lg btn-success" value="Actualizar" id="submit">
        <input type="hidden" name="action" value="edit">
        <input type="hidden" name="id_cliente" value="<?php echo $data['id']; ?>">
          <button type="button" class="btn  btn-lg  btn-default" data-dismiss="modal">Cancelar</button>
        </div>
        </form>

      
</div>



</div>
<!-- /container -->





<?php require('include/footer.php');?>