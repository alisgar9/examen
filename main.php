<?php
require('include/header.php');
?>

<div class="container text-center">
<h1>Acceso al Sistema</h1>
</div> <!-- /container -->





<!-- Login Form -->
<div class="container">
      <form action="submit.php" method="post" name="login_form" id="login_form" autocomplete="off">

        <label for="Email" class="sr-only">Correo Electronico</label>
        <input type="email" name="Email" id="Email" class="form-control" placeholder="Correo Electronico" required autofocus>

        <label for="Password" class="sr-only">Contraseña</label>
        <input type="password" name="Password" id="Password" class="form-control" placeholder="Contraseña" required pattern=".{6,12}" title="6 to 12 characters.">

        <div id="display_error" class="alert alert-danger fade in"></div>

        <button type="submit" class="btn btn-lg btn-primary btn-block">Acceder</button>
      </form>

      <div id="map"></div>   

</div>

 
	

<?php require('include/footer.php');?>