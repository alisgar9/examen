<link href="./css/bootstrap.min.css" rel="stylesheet">
<link href="./css/style.css" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,300,600">


<script src="./js/jquery.min.js" async></script>
<script src="./js/bootstrap.min.js" async></script>
<script src="./js/app.js" async></script>

</body>
</html>
